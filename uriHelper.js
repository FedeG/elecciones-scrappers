var Q = require('q');
var open = require("open-uri");

module.exports.doGet = function(uri){

    var def = Q.defer();
    open(uri,function(err,buffer){

        var html = buffer.toString("utf-8");

        if(err){
            def.reject(err, html);
        }
        else{
            def.resolve(html);
        }
    });

    return def.promise;
};