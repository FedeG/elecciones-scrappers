var cheerio = require('cheerio');
var http    = require('request-promise');
var config    = require('./config');

function getJSONAndPost($, depto, name){
    changeDepto($, depto)
      .then(function (html) {
          var jsonDepto = {
              "codigoProvincia": "20",
              "nombreProvincia": "Santa Cruz",
              "nombreDepartamento": name,
              "codigoMunicipio": "999",
              "elecciones": {"general": []}
          };
          jsonDepto.codigoDepartamento = depto.codigo.toString();
          var $ = cheerio.load(html);
          for (var eleccion in depto.elecciones)
              jsonDepto.elecciones.general.push(getEleccion($, depto, eleccion));
          postJson(jsonDepto);
      });
}

function postJson(jsonDepto){
    var options = {
        method: 'POST',
        uri: config.urls.scrappingService,
        body: jsonDepto,
        json: true
    };

    http(options)
      .then(function(){
        console.log("Post to scrapping service OK")
      }, function err(err) {
        console.log("ERROR in post to scrapping service", err)
      });
}

function changeDepto($, depto){
    var option = $(config.menuID).children()[depto.option];
    return http.get(config.urls.deptoURL + option.attribs.value + '.php');
}

function getEleccion($, depto, eleccion){
    var jsonEleccion = {};
    jsonEleccion.nombre = eleccion;
    var eleccionConfig = depto.elecciones[eleccion];
    var cheerioEleccion = changeEleccion($, eleccionConfig);
    addResumen(cheerioEleccion, jsonEleccion);
    addPartidos(cheerioEleccion, jsonEleccion);
    return jsonEleccion;
}

function addPartidos($, jsonEleccion){
    jsonEleccion.partidos = [];
    var partidos = $('.pai').children('.datos');
    for (var i = 0; i < partidos.length; i++) {
        var partido = partidos.eq(i).children('.contenedor-barra');
        var titulo = partido.children('.titulo').html();
        var porcentaje = partido.children('.porcentaje').html();
        if (porcentaje.indexOf('span') !== -1) {
            porcentaje = porcentaje.split('<span>')[1];
        }
        porcentaje = parseFloat(parseFloat(porcentaje).toFixed(2));
        var votos = parseInt(partido.children('.cantidad2').html().split(' ')[0].split('(')[1]);
        var jsonPartido = {
            partido: {
                nombre: titulo.split('/')[1],
                color: "c0c0c0",
                orden: i,
                codigo: titulo.split('/')[0],
                sublema: []
            },
            votos: {
                votos: (votos) ? votos:0,
                porcentaje: (porcentaje) ? porcentaje:0
            }
        };
        addSublemas(partido, jsonPartido);
        jsonEleccion.partidos.push(jsonPartido);
    }
}

function addSublemas(partido, jsonPartido){
    var sublemas = partido.children('.listas').children('.sublemas');
    if (sublemas.length > 0){
        for (var j = 0; j < sublemas; i++) {
            jsonPartido.partido.sublema.push({
                "sublema_nombre1": sublema.title(),
                "sublema_nombre2": sublema.children('.partidos').html(),
                "votos_votos": parseInt(sublema.children('.cantidad').html().split(' ')[0].split('(')[1]),
                "votos_porcentaje": parseFloat(parseFloat(sublema.children('.porcentaje2').html()).toFixed(2))
            })
        }
    }
}

function addResumen($, jsonEleccion){
    jsonEleccion.resumen = {};
    for (var item in config.resumen){
        if (item === 'cantidades'){
            jsonEleccion.resumen[item] = {};
            for (var itemCantidad in config.resumen[item]){
                jsonEleccion.resumen[item][itemCantidad] = {};
                jsonEleccion.resumen[item][itemCantidad]['votos'] = getValue($, config.resumen[item][itemCantidad]['votos']);
                if (itemCantidad !== 'electores') jsonEleccion.resumen[item][itemCantidad]['porcentaje'] = getValue($, config.resumen[item][itemCantidad]['porcentaje']);
            }
        }
        else jsonEleccion.resumen[item] = getValue($, config.resumen[item]);
    }
}

function changeEleccion($, eleccion){
    return cheerio.load($(eleccion.tab).html());
}

function getValue($, way){
    var table = $(way.id).children('.totales').eq(way.childrens[0]);
    var value = table.children('.tr').eq(way.childrens[1]);
    value = value.children((way.childrens[2]==1) ? '.cuadro1':'.cuadro2');
    value = value.html();
    if (value || value.length < 1) return 0;
    if (way.sum) value += getValue($, {id: way.id, parse: way.parse, childrens: way.sum});
    if (way.parse === 'int') value = parseInt(value);
    else if (way.parse === 'float') value = parseFloat(parseFloat(value).toFixed(2));
    return value;
}

module.exports.doScrapping = function() {
    http.get(config.urls.baseURL).then(function (html) {
        var $ = cheerio.load(html);
        for (var depto in config.deptos)
            getJSONAndPost($, config.deptos[depto], depto);
    });
};